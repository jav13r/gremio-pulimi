"use strict";
const pulumi = require("@pulumi/pulumi");
const aws = require("@pulumi/aws");
const awsx = require("@pulumi/awsx");

const env = pulumi.getStack();



// Export the name of the bucket
// module.exports = function () {
//     // Create an AWS resource (S3 Bucket)
//     const bucket = new aws.s3.Bucket(`${env}-my-test-bucket`, {
//         bucket: `${env}-my-test-bucket`,
//     });

//     return {
//         bucketArn: bucket.arn,
//         bucketName: bucket.bucket,
//     }
// }



module.exports = function () {
    const dynamo = new aws.dynamodb.Table(`${env}-test-dynamo`, {
        name: `${env}-test-dynamo`,
        billingMode: "PAY_PER_REQUEST",
        attributes: [
            { name: "id", type: "S" },
            { name: "age_color_sparse", type: "S" },
            { name: "type_user", type: "N" },
        ],
        tags: {
            Environment: env,
        },
        hashKey: "id",
        globalSecondaryIndexes: [
            {
                name: "by_age_color_sparse",
                projectionType: "ALL",
                hashKey: "age_color_sparse",
            },
            {
                name: "by_type_user",
                projectionType: "ALL",
                hashKey: "type_user",
            }
        ],
    });

    return {
        dynamoArn: dynamo.arn,
        dynamoName: dynamo.name,
    }
}